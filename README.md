# Project Description
## Summary
In a collaboration with the Archaeological Museum of Thessaloniki in Greece, I designed and developed a game (and specifically the “Treasure Hunt”) for touring the museum, using several Gamification mechanics. A few characteristics of the app were Augmented Reality, Indoor Navigation, Social Networking and Media presentation. Some of the technologies I used were Augmented Reality (Metaio SDK), Graphics(OpenCL) and several Web Services such as Maps and Facebook.
## Supervisor
Athena Vakali
## Student
Konstantinos Chartomatzis
## Images
![Screen Shot 2015-01-25 at 20.52.35.png](https://bitbucket.org/repo/G65qMk/images/2453444654-Screen%20Shot%202015-01-25%20at%2020.52.35.png)
![Screen Shot 2015-01-25 at 21.21.59.png](https://bitbucket.org/repo/G65qMk/images/1720778402-Screen%20Shot%202015-01-25%20at%2021.21.59.png)
![Screen Shot 2015-01-25 at 20.52.19.png](https://bitbucket.org/repo/G65qMk/images/3328769852-Screen%20Shot%202015-01-25%20at%2020.52.19.png)
![Screen Shot 2015-01-25 at 21.22.16.png](https://bitbucket.org/repo/G65qMk/images/1598356147-Screen%20Shot%202015-01-25%20at%2021.22.16.png)