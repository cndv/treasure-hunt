//
//  ShortestPathStep.h
//  CatMaze
//
//  Created by Kostas Chart on 12/18/12.
//  Copyright (c) 2012 Ray Wenderlich. All rights reserved.
//

@interface ShortestPathStep : NSObject
{
	CGPoint position;
	int gScore;
	int hScore;
    ShortestPathStep *parent;
}
@property (nonatomic) CGPoint position;
@property (nonatomic) int gScore;
@property (nonatomic) int hScore;
@property (nonatomic, strong) ShortestPathStep *parent;

- (id)initWithPosition:(CGPoint)pos;
- (int)fScore;

@end