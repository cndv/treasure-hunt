#import "ViewController.h"

@interface ViewController ()


@end

@implementation ViewController


//==============================================================================================
//
//  extended the viewDidLoad Method

- (void)viewDidLoad {
    [super viewDidLoad];    
    
    // load our tracking configuration
    NSString* trackingDataFile = [[NSBundle mainBundle] pathForResource:@"TrackingData_MarkerlessFast" ofType:@"xml" inDirectory:@"AllAssets"];
    
    
    // if you want to test the 3D tracking, please uncomment the line below and comment the line above
    //NSString* trackingDataFile = [[NSBundle mainBundle] pathForResource:@"TrackingData_ML3D" ofType:@"xml" inDirectory:@"Assets"];
    
    
	if(trackingDataFile)
	{
		bool success = m_metaioSDK->setTrackingConfiguration([trackingDataFile UTF8String]);
		if (!success)
			NSLog(@"No success loading the tracking configuration");
	}
    
    
    
//    // load content
//    NSString* modelPath = [[NSBundle mainBundle] pathForResource:@"metaioman" ofType:@"md2" inDirectory:@"AllAssets"];
//
//	if(modelPath)
//	{
//		// if this call was successful, theLoadedModel will contain a pointer to the 3D model
//        m_metaioMan =  m_metaioSDK->createGeometry([modelPath UTF8String]);
//        
//#pragma mark - This is needed for multiple trasking images
//        m_metaioMan->setCoordinateSystemID(1);
//
//        if (m_metaioMan)
//        {
//# pragma mark - Notification center, implementation of the delegates method
//            // scale it a bit up
//            m_metaioMan->setScale(metaio::Vector3d(4.0,4.0,4.0));
//        }
//        else
//        {
//            NSLog(@"error, could not load %@", modelPath);
//        }
//    }
    
    
    // Load carseat
    NSString* cubeModel = [[NSBundle mainBundle] pathForResource:@"cubeBlend2" ofType:@"md2" inDirectory:@"Assets1"];
    if(cubeModel) {
        // if this call was successful, theLoadedModel will contain a pointer to the 3D model
        metaio::IGeometry* theLoadedCarseatModel =  m_metaioSDK->createGeometry([cubeModel UTF8String]);
        
        theLoadedCarseatModel->setCoordinateSystemID(2);
        
        if( theLoadedCarseatModel ) {
            // scale it a bit up1
            theLoadedCarseatModel->setScale(metaio::Vector3d(.5,.5,.5));
        } else {
            NSLog(@"error, could not load %@", cubeModel);
        }
    }

    
//    // loadimage
//    
//    NSString* imagePath = [[NSBundle mainBundle] pathForResource:@"frame" ofType:@"png" inDirectory:@"AllAssets"];
//    
//    if (imagePath)
//    {
//        m_imagePlane = m_metaioSDK->createGeometryFromImage([imagePath UTF8String]);
//        if (m_imagePlane)
//        {
////            m_imagePlane->setCoordinateSystemID(2);
//            if (m_imagePlane)
//            {
//                m_imagePlane->setScale(metaio::Vector3d(4.0,4.0,4.0));
//            }
//        }
//        else NSLog(@"Error: could not load image plane");
//    }
    
    // load the movie plane
    NSString* moviePath = [[NSBundle mainBundle] pathForResource:@"demo_movie" ofType:@"3g2" inDirectory:@"AllAssets"];
    
	if(moviePath)
	{
        m_moviePlane =  m_metaioSDK->createGeometryFromMovie([moviePath UTF8String], true); // true for transparent movie
        if( m_moviePlane)
        {
            m_moviePlane->setCoordinateSystemID(1);
            m_moviePlane->setScale(metaio::Vector3d(2.0,2.0,2.0));
//            m_moviePlane->setCoordinateSystemID(2);
            m_moviePlane->setRotation(metaio::Rotation(metaio::Vector3d(0, 0, -M_PI_2)));
            m_moviePlane->startMovieTexture(YES);
            
        }
        else
        {
            NSLog(@"Error: could not load movie planes");
        }
    }
    
    
    
    // start with metaio man
   
}

/*
#pragma mark - App Logic
- (void) setActiveModel: (int) modelIndex
{
    switch ( modelIndex )
    {
        case 0:
        {
            m_imagePlane->setVisible(false);
            m_metaioMan->setVisible(true);
            
            // stop the movie
            m_moviePlane->setVisible(false);
            m_moviePlane->stopMovieTexture();
        }
            break;
            
            
        case 1:
            m_imagePlane->setVisible(true);
            m_metaioMan->setVisible(false);
            
            // stop the movie
            m_moviePlane->setVisible(false);
            m_moviePlane->stopMovieTexture();
            break;
            
            
        case 2:
            m_imagePlane->setVisible(false);
            m_metaioMan->setVisible(false);
            
            m_moviePlane->setVisible(true);
            m_moviePlane->startMovieTexture(true); // loop = true
            break;
    }
    
}
*/

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    // allow rotation in all directions
    return YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}



- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
   
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (m_moviePlane->isVisible())
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"metaioman" object:self];
        [self.delegate viewControllerDelegate:self foundObject:TRUE];
    }
}



//==============================================================================================
//
//  didReceiveMemoryWarning

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end