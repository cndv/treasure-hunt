//
//  LLAViewController.h
//  demoMus
//
//  Created by Kostas Chart on 11/28/12.
//  Copyright (c) 2012 Kostas Tzis. All rights reserved.
//


#import <CoreLocation/CoreLocation.h>
#import <CoreMotion/CoreMotion.h>
#import "MetaioSDKViewController.h"

namespace metaio
{
    class IGeometry;   // forward declaration
}

@interface LLAViewController : MetaioSDKViewController <CLLocationManagerDelegate>
{
    metaio::IBillboardGroup*   billboardGroup;   //!< Our default billboard group
    metaio::IGeometry* southBillboard;
    metaio::IGeometry* northBillBoard;
    metaio::IGeometry* metaioMan;
    metaio::IRadar* m_radar;
    
    UIImage* northImage;
    UIImage* southImage;
}
@property (nonatomic, retain) CLLocation* currentLocation;				//!< Contains the current location




@end
