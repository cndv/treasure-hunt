//
//  main.m
//  demoMus
//
//  Created by Kostas Chart on 11/22/12.
//  Copyright (c) 2012 Kostas Tzis. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
