
#import "MetaioSDKViewController.h"
#import "EAGLView.h"

@class ViewController;

@protocol ViewControllerDelegate <NSObject>
@optional
- (void) viewControllerDelegate: (ViewController *)sender foundObject: (BOOL) found;

@end

@interface ViewController : MetaioSDKViewController {
    metaio::IGeometry*       m_imagePlane;           // pointer to the image plane
    metaio::IGeometry*       m_metaioMan;            // pointer to the metaio man model
    metaio::IGeometry*       m_moviePlane;           // pointer to our movie plane
    
}


@property (nonatomic, weak) id <ViewControllerDelegate> delegate;


@end
