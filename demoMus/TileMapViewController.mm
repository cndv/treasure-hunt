//
//  TileMapViewController.m
//  demoMus
//
//  Created by Costa Hart on 2/23/13.
//  Copyright (c) 2013 Kostas Tzis. All rights reserved.
//
#import "HelloWorldLayer.h"
#import <dispatch/dispatch.h>
#import "TileMapViewController.h"
#import "CCScrollView.h"

@interface TileMapViewController ()

@end

@implementation TileMapViewController


-(void) viewWillAppear:(BOOL)animated
{
    CCDirectorIOS* director = (CCDirectorIOS*)[CCDirector sharedDirector];
    //[director startAnimation];
    [director runWithScene:[HelloWorldLayer scene]];

}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // initialize cocos2d director
	CCDirectorIOS* director = (CCDirectorIOS*)[CCDirector sharedDirector];
    
	director.wantsFullScreenLayout = NO;
	director.projection = kCCDirectorProjection2D;
	director.animationInterval = 1.0 / 60.0;
	director.displayStats = YES;
	[director enableRetinaDisplay:YES];
	
	NSArray* subviews = self.view.subviews;
	for (int i = 0; i < subviews.count; i++)
	{
		UIView* subview = [subviews objectAtIndex:i];
		if ([subview isKindOfClass:[CCGLView class]])
		{
			director.view = (CCGLView*)subview;
            
            [director.view setMultipleTouchEnabled:YES];
            scrollView = [[CCScrollView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
            scrollView.contentSize = CGSizeMake(1000, 981);
            [scrollView setUserInteractionEnabled:TRUE];
            [scrollView setScrollEnabled:TRUE];
            [scrollView setDelegate:(id<UIScrollViewDelegate>)scrollView];
           
            [self.view addSubview:scrollView];
            
			[director runWithScene:[HelloWorldLayer scene]];
			break;
		}
	}
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
