//
//  MapViewController.h
//  demoMus
//
//  Created by Kostas Chart on 12/1/12.
//  Copyright (c) 2012 Kostas Tzis. All rights reserved.
//


@interface MapViewController : UIViewController <UIScrollViewDelegate>
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;

@end
