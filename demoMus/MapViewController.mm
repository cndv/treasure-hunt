//
//  MapViewController.m
//  demoMus
//
//  Created by Kostas Chart on 12/1/12.
//  Copyright (c) 2012 Kostas Tzis. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()
@property (nonatomic, strong) UIImageView *imageView;
//    - (void)centerScrollViewContents;
//    - (void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer;
//    - (void)scrollViewTwoFingerTapped:(UITapGestureRecognizer*)recognizer;
@end

@implementation MapViewController


- (UIImage *)imageByDrawingCircleOnImage:(UIImage *)image
{
        //// General Declarations
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        //// Color Declarations
        UIColor* fillColor = [UIColor colorWithRed: 0.343 green: 0.343 blue: 1 alpha: 1];
        UIColor* strokeColor = [UIColor colorWithRed: 0 green: 0 blue: 0 alpha: 1];
        UIColor* color = [UIColor colorWithRed: 0 green: 0.561 blue: 1 alpha: 0.204];
        UIColor* color3 = [UIColor colorWithRed: 0.336 green: 0.415 blue: 0.792 alpha: 0.894];
        
        //// Shadow Declarations
        UIColor* shadow = strokeColor;
        CGSize shadowOffset = CGSizeMake(2*0.1, -0.1*2);
        CGFloat shadowBlurRadius = 2*5;
        UIColor* shadow2 = strokeColor;
        CGSize shadow2Offset = CGSizeMake(2*1.1, 2*1.1);
        CGFloat shadow2BlurRadius = 2*5;
    
    //// Group
    {
	// begin a graphics context of sufficient size
	UIGraphicsBeginImageContext(image.size);
    
	// draw original image into the context
	[image drawAtPoint:CGPointZero];
    
    
    //// Bezier Drawing
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint: CGPointMake(2*163.5, 2*422.5)];
    [bezierPath addCurveToPoint: CGPointMake(2*65.5, 2*410.5) controlPoint1: CGPointMake(2*65.5, 2*410.5) controlPoint2: CGPointMake(2*65.5, 2*410.5)];
    [bezierPath addLineToPoint: CGPointMake(2*47.5, 2*231.5)];
//    [bezierPath addLineToPoint: CGPointMake(2*57.5, 2*81.5)];
//    [bezierPath addLineToPoint: CGPointMake(2*163.5, 2*59.5)];
//    [bezierPath addLineToPoint: CGPointMake(2*260.5, 2*70.5)];
//    [bezierPath addLineToPoint: CGPointMake(2*269.5, 2*231.5)];
//    [bezierPath addLineToPoint: CGPointMake(2*255.5, 2*410.5)];
//    [bezierPath addLineToPoint: CGPointMake(2*163.5, 2*422.5)];
    CGContextSaveGState(context);
    CGContextSetShadowWithColor(context, shadow2Offset, shadow2BlurRadius, shadow2.CGColor);
    [color3 setStroke];
    bezierPath.lineWidth = 3*4.5;
    [bezierPath stroke];
    CGContextRestoreGState(context);
    
    
        //// Triangle Drawing
        UIBezierPath* trianglePath = [UIBezierPath bezierPath];
        CGFloat topX = 2*47.5;
        CGFloat topY = 2*231.5;
        CGFloat leftX = topX - 20.58;
        CGFloat leftY = topY + 32;
        CGFloat rightX = topX + 17.42;
        CGFloat rightY = leftY;
        [trianglePath moveToPoint: CGPointMake(leftX, leftY)];//left
        [trianglePath addLineToPoint: CGPointMake(topX, topY)];//top
        [trianglePath addLineToPoint: CGPointMake(rightX, rightY)];//right
        [trianglePath addLineToPoint: CGPointMake(leftX, leftY)];//left
        [trianglePath closePath];
        trianglePath.miterLimit = 11;
        
        [color3 setFill];
        [trianglePath fill];
        [color3 setStroke];
        trianglePath.lineWidth = 5.5;
        [trianglePath stroke];
   
    
    
    }
    

    CGFloat outerOvalW = 2*62;
    CGFloat outerOvalH = 2*65;
    CGFloat outerOvalCX = 2*47.5; // center X
    CGFloat outerOvalCY= 2*231.5; // center Y
    CGFloat outerOvalX = outerOvalCX - outerOvalW/2;
    CGFloat outerOvalY = outerOvalCY - outerOvalH/2;
    //    CGFloat outerOvalCX = ovalX + ovalW/2;
    //    CGFloat outerOvalCY = ovalY + ovalH/2;
    
    //// Oval 2 Drawing
    UIBezierPath* oval2Path = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(outerOvalX, outerOvalY, outerOvalW, outerOvalH)];
    [color setFill];
    [oval2Path fill];
    [strokeColor setStroke];
    oval2Path.lineWidth = 1;
    [oval2Path stroke];


    CGFloat ovalW = 2*14;
    CGFloat ovalH = 2*16;
    CGFloat ovalCX = 2*47.5; // center X
    CGFloat ovalCY= 2*231.5; // center Y
    CGFloat ovalX = ovalCX - ovalW/2;
    CGFloat ovalY = ovalCY - ovalH/2;
//    CGFloat ovalCX = ovalX + ovalW/2;
//    CGFloat ovalCY = ovalY + ovalH/2;
    
    //// Oval Drawing
    UIBezierPath* ovalPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(ovalX, ovalY, ovalW, ovalH)];
    CGContextSaveGState(context);
    CGContextSetShadowWithColor(context, shadowOffset, shadowBlurRadius, shadow.CGColor);
    [fillColor setFill];
    [ovalPath fill];
    
    ////// Oval Inner Shadow
    CGRect ovalBorderRect = CGRectInset([ovalPath bounds], -shadowBlurRadius, -shadowBlurRadius);
    ovalBorderRect = CGRectOffset(ovalBorderRect, -shadowOffset.width, -shadowOffset.height);
    ovalBorderRect = CGRectInset(CGRectUnion(ovalBorderRect, [ovalPath bounds]), -1, -1);
    
    UIBezierPath* ovalNegativePath = [UIBezierPath bezierPathWithRect: ovalBorderRect];
    [ovalNegativePath appendPath: ovalPath];
    ovalNegativePath.usesEvenOddFillRule = YES;
    
    CGContextSaveGState(context);
    {
        CGFloat xOffset = shadowOffset.width + round(ovalBorderRect.size.width);
        CGFloat yOffset = shadowOffset.height;
        CGContextSetShadowWithColor(context,
                                    CGSizeMake(xOffset + copysign(0.1, xOffset), yOffset + copysign(0.1, yOffset)),
                                    shadowBlurRadius,
                                    shadow.CGColor);
        
        [ovalPath addClip];
        CGAffineTransform transform = CGAffineTransformMakeTranslation(-round(ovalBorderRect.size.width), 0);
        [ovalNegativePath applyTransform: transform];
        [[UIColor grayColor] setFill];
        [ovalNegativePath fill];
    }
    CGContextRestoreGState(context);
    
    CGContextRestoreGState(context);
    
    [strokeColor setStroke];
    ovalPath.lineWidth = 1;
    [ovalPath stroke];
    
   


    // make image out of bitmap context
	UIImage *retImage = UIGraphicsGetImageFromCurrentImageContext();

	// free the context
	UIGraphicsEndImageContext();
	return retImage;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)centerScrollViewContents
//{
//    CGSize boundsSize = self.scrollView.bounds.size;
//    CGRect contentsFrame = self.imageView.frame;
//    
//    if (contentsFrame.size.width < boundsSize.width) {
//        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
//    } else {
//        contentsFrame.origin.x = 0.0f;
//    }
//    
//    if (contentsFrame.size.height < boundsSize.height) {
//        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
//    } else {
//        contentsFrame.origin.y = 0.0f;
//    }
//    
//    self.imageView.frame = contentsFrame;
//}
//
//- (void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer
//{
//    // 1
//    CGPoint pointInView = [recognizer locationInView:self.imageView];
//    
//    // 2
//    CGFloat newZoomScale = self.scrollView.zoomScale * 1.5f;
//    newZoomScale = MIN(newZoomScale, self.scrollView.maximumZoomScale);
//    
//    // 3
//    CGSize scrollViewSize = self.scrollView.bounds.size;
//    
//    CGFloat w = scrollViewSize.width / newZoomScale;
//    CGFloat h = scrollViewSize.height / newZoomScale;
//    CGFloat x = pointInView.x - (w / 2.0f);
//    CGFloat y = pointInView.y - (h / 2.0f);
//    
//    CGRect rectToZoomTo = CGRectMake(x, y, w, h);
//    
//    // 4
//    [self.scrollView zoomToRect:rectToZoomTo animated:YES];
//}
//
////FIX to zoom in/ out
//
//- (void)scrollViewTwoFingerTapped:(UITapGestureRecognizer*)recognizer
//{
//    // Zoom out slightly, capping at the minimum zoom scale specified by the scroll view
////    CGFloat newZoomScale = self.scrollView.zoomScale / 1.5f;
////    newZoomScale = MAX(newZoomScale, self.scrollView.minimumZoomScale);
////    [self.scrollView setZoomScale:newZoomScale animated:YES];
//}
//
// viewForZoomingInScrollView
/*
- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    // Return the view that you want to zoom
    return self.imageView;
}
*/

//- (void)scrollViewDidZoom:(UIScrollView *)scrollView
//{
//    // The scroll view has zoomed, so you need to re-center the contents
//    [self centerScrollViewContents];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 1
//    NSString* imagePath;
//    imagePath = [[NSBundle mainBundle] pathForResource:@"coloredMap" ofType:@"png" inDirectory:@"Recources"];
//    UIImage *image =  [[UIImage alloc] initWithContentsOfFile:imagePath];
    
    //FIX image path , load the image from resources
    
    UIImage *image = [UIImage imageNamed:@"IsogeioKatoOrofos@2x.png"];
    
    
    // call circle drawing
    UIImage *imageWithCircle = [self imageByDrawingCircleOnImage:image];
    
    
    self.imageView = [[UIImageView alloc] initWithImage:imageWithCircle];
    self.imageView.frame = self.view.frame;
    //self.imageView.frame = (CGRect){.origin=CGPointMake(0.0f, 0.0f), .size=image.size};
    [self.scrollView addSubview:self.imageView];
    // 2
    self.scrollView.contentSize = self.imageView.frame.size;
    
//    // 3
//    UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDoubleTapped:)];
//    doubleTapRecognizer.numberOfTapsRequired = 2;
//    doubleTapRecognizer.numberOfTouchesRequired = 1;
//    [self.scrollView addGestureRecognizer:doubleTapRecognizer];
//    
//    UITapGestureRecognizer *twoFingerTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTwoFingerTapped:)];
//    twoFingerTapRecognizer.numberOfTapsRequired = 1;
//    twoFingerTapRecognizer.numberOfTouchesRequired = 2;
//    [self.scrollView addGestureRecognizer:twoFingerTapRecognizer];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // 4
//    CGRect scrollViewFrame = self.scrollView.frame;
//    CGFloat scaleWidth = scrollViewFrame.size.width / self.scrollView.contentSize.width;
//    CGFloat scaleHeight = scrollViewFrame.size.height / self.scrollView.contentSize.height;
//    CGFloat minScale = MIN(scaleWidth, scaleHeight);
//    self.scrollView.minimumZoomScale = minScale;
//    
//    // 5
//    self.scrollView.maximumZoomScale = 1.0f;
//    self.scrollView.zoomScale = minScale;
    
    // 6
//    [self centerScrollViewContents];
}

@end
