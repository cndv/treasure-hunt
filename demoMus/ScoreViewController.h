//
//  ScoreViewController.h
//  demoMus
//
//  Created by Kostas Chart on 11/30/12.
//  Copyright (c) 2012 Kostas Tzis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoreViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *score;
@property (weak, nonatomic) NSNumber *scorePoints;

@end
