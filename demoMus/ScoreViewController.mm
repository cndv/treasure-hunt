//
//  ScoreViewController.m
//  demoMus
//
//  Created by Kostas Chart on 11/30/12.
//  Copyright (c) 2012 Kostas Tzis. All rights reserved.
//

#import "ScoreViewController.h"
#import "ViewController.h"

@interface ScoreViewController() <ViewControllerDelegate>

@end

@implementation ScoreViewController 

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(handleNotification:) name:@"metaioman" object:nil];
//    [self.score setNeedsDisplay];
//    [self.view setNeedsDisplay];
    
}


-(void)viewDidUnLoad{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"metaioman"
                                                  object:nil];
    
}


- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)handleNotification:(NSNotification *)notification
{
//    if (notification.)
    self.scorePoints = @1000;
    NSString *msg = @"Score: ";
    NSString *newMsg = [msg stringByAppendingString:[self.scorePoints stringValue]];
    self.score.text = newMsg ;
    
    //now you have a reference to the object that was passed from your app delegate
}
@end
