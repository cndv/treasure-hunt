//
//  AnimeViewController.h
//  demoMus
//
//  Created by Kostas Chart on 11/28/12.
//  Copyright (c) 2012 Kostas Tzis. All rights reserved.
//


#import "MetaioSDKViewController.h"
@class AnimeViewController;

@protocol AnimeViewControllerDelegate <NSObject>
    @optional
    - (void) animeViewControllerDelegate: (AnimeViewController *)sender foundObject: (BOOL) found;

@end

@interface AnimeViewController : MetaioSDKViewController
    {
        metaio::IGeometry*       m_metaioMan;            // pointer to the metaio man model
        bool                     m_isCloseToModel;     // to keep track if we moved close to the
    }

    @property (nonatomic, weak) id <AnimeViewControllerDelegate> delegate;

    /** This method is regularly called, calculates the distance between phone and target
     * and performs actions based on the distance
     */
    - (void) checkDistanceToTarget;
@end
