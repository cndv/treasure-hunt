// Copyright 2007-2012 metaio GmbH. All rights reserved.
//
//  AS_IARELEngine.h
//  AS_MetaioWorldClient
//
//  Created by Stefan Misslinger on 1/20/12.
//  Copyright (c) 2012 metaio, Inc. All rights reserved.
//

#ifndef AS_MetaioWorldClient_AS_IARELEngine_h
#define AS_MetaioWorldClient_AS_IARELEngine_h

#include <metaioSDK/MobileStructs.h>
#include <metaioSDK/TrackingValues.h>
#include <metaioWorldClient/AS_MetaioWorld_DataStructures.h>


namespace metaio
{
namespace world
{
class MetaioWorldPOI;            // fwd decl

/**
 * \brief General interface for AREL Engine
 */
class IARELEngine
{
public:

	/** Destructor
	 */
	virtual ~IARELEngine() {};

	/** Inform the engine that a user clicked on a POI
	 * \param theObject the object that was clicked on
	 */
	virtual void onObjectTouchDown(MetaioWorldPOI* theObject) = 0;

	/** Inform the engine that a user touch-up'ed on a POI
	 * \param theObject the object that was touched
	 */
	virtual void onObjectTouchUp(MetaioWorldPOI* theObject) = 0;


	/** Inform the engine that tracking values have arrived
	 * \param trackingValues the current tracking values
	 */
	virtual void onTrackingEvent(std::vector<metaio::TrackingValues> trackingValues) = 0;


	/** This method is called after all POIs have been added
	 * This call is necessary to be able to start all IDLE events
	 *
	 */
	virtual void onSceneReady() = 0;


	/** Use this method to let the engine process AREL
	 * \param url the AREL URL
	 * \return false if there was an error processing the call
	 */
	virtual bool processAREL(std::string url) = 0;

	/** Called when an animation ended
	 * \param object the object that triggered the animation
	 * \param animationName the name of the animation that finished
	 */
	virtual void onAnimationEnd(const metaio::world::MetaioWorldPOI* object, std::string animationName) = 0;

	/** Called when a movie ended
	 * \param object the object that contains the movie
	 * \param movieName the name of the movie that finished
	 */
	virtual void onMovieEnded(const metaio::world::MetaioWorldPOI* object, std::string movieName) = 0;

	/** Inform the engine the loading a specific poi has been completed
	 * \param object the poi
	 * \param success true if loading was successful, false otherwise
	 */
	virtual void onLoadingPOIComplete(const  metaio::world::MetaioWorldPOI* object, bool success) = 0;


	/** Inform the engine the loading of a specific poi has been started
	 * \param object the poi
	 */
	virtual void onStartLoadingPOI(const  metaio::world::MetaioWorldPOI* object) = 0;


	/** Inform the engine the a specific poi will be removed
	 * \param object
	 */
	virtual void onWillRemoveObject(MetaioWorldPOI* object) = 0;


	/** Inform the engine that we will remove all content (usually reloading everything)
	 */
	virtual void onWillRemoveContent() = 0;


	/** Call this method to trigger media events when closing websites, videos or sound playback is complete
	 * \param itemID the id of the item
	 * \param event the event
	 */
	virtual void callMediaEvent(const std::string& itemID, EAREL_MEDIA_EVENT event) = 0;


	/**
	 * \brief Callback that delivers the next camera image.
	 *
	 * The image will have the dimensions of the current capture resolution.
	 * To request this callback, call requestCameraFrame()
	 *
	 * \param cameraFrame the latest camera image
	 *
	 * \return true, if the frame was handled and does not need to be forwarded to the delegate

	 * \note you must copy the ImageStuct::buffer, if you need it for later.
	 */
	virtual bool onNewCameraFrame(metaio::ImageStruct*  cameraFrame) = 0;


	/** Inform the engine that the position has been updated
	 * \param position the position
	 */
	virtual void onDidUpdatePosition(metaio::LLACoordinate position) = 0;


	/** Switch an internal flag that will allow forwarding events to the AREL webbrowser
	 * \param ready true if switching to ready, false otherwise
	 */
	virtual void setEngineIsReady(bool ready) = 0;
	
	/**
	 * The visual search callback.
	 *
	 * This method is always called after you successfully started a new visual search
	 * (with IUnifeyeMobile::performVisualSearch()) and received the result from the server.
	 *
	 * \param success If true, the search was successful and the found patterns will be
	 *	stored in the response vector. If false, the response vector will be empty and
	 *	the errorMsg parameter will contain the error reason
	 * \param errorMsg the error reason if the visual search wasn't successful
	 * \param response All found results. The result which is most probably the one you
	 * search for is stored at vector position 0.
	 */
	virtual void onVisualSearchResult(bool success, const std::string& errorMsg,
	                                  const std::vector< metaio::VisualSearchResponse >& response){};
};

}
}


#endif
