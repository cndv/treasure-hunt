// Copyright 2007-2012 metaio GmbH. All rights reserved.
//
//  AS_AREL_JunaioAPI1Engine.h
//  AS_MetaioWorldClient
//
//  Created by Stefan Misslinger on 1/20/12.
//  Copyright (c) 2012 metaio, Inc. All rights reserved.
//

#ifndef AS_MetaioWorldClient_AS_AREL_JunaioAPI1Engine_h
#define AS_MetaioWorldClient_AS_AREL_JunaioAPI1Engine_h

#include <metaioWorldClient/AS_IARELEngine.h>
#include <string>
#include <map>
#include <metaioWorldClient/AS_MetaioWorld_DataStructures.h>

namespace metaio
{
namespace common
{
class ITimer;	// fwd decl
}

namespace world
{
class MetaioWorldPOI;            // fwd decl
class MetaioWorldPOIManager;     // fwd decl
class JunaioDataSource;        // fwd decl
class IMetaioWorldPOIManagerDelegate;   // fwd decl

/** Engine that handles the old API
 */
class JunaioAPI1Engine  : public IARELEngine
{
public:


	/** Constructor
	 * \param pPOIManager pointer to poi manager
	 * \param pDatasource pointer to datasource
	 * \param pPOIManagerDelegate pointer to poimanager delegate
	 */
	JunaioAPI1Engine(
	    metaio::world::MetaioWorldPOIManager*            pPOIManager,
	    metaio::world::JunaioDataSource*               pDatasource,
	    metaio::world::IMetaioWorldPOIManagerDelegate* 	pPOIManagerDelegate
	);

	/** Destructor
	 */
	virtual ~JunaioAPI1Engine();


	/** Inform the engine that a user clicked on a POI
	 * \param theObject the object that was clicked on
	 */
	void onObjectTouchDown(MetaioWorldPOI* theObject);

	/** Inform the engine that a user touch-up'ed on a POI
	 * \param theObject the object that was touched
	 */
	void onObjectTouchUp(MetaioWorldPOI* theObject) {};


	/** Inform the engine that tracking values have arrived
	 * \param trackingValues the current tracking values
	 */
	void onTrackingEvent(std::vector< metaio::TrackingValues > trackingValues);


	/** This method is called after all POIs have been added
	 * This call is necessary to be able to start all IDLE events
	 *
	 */
	void onSceneReady();

	/** Execute AREL
	 * \param url the URL
	 * \return always returns false as AREL is not compatible with API1 engine
	 */
	bool processAREL(std::string url) { return false; };

	/** Called when an animation ended
	 * \param object the object that triggered the animation
	 * \param animationName the name of the animation that finished
	 */
	void onAnimationEnd(const  metaio::world::MetaioWorldPOI* object, std::string animationName);


	void onMovieEnded(const metaio::world::MetaioWorldPOI* object, std::string movieName) {};

	/** Inform the engine the loading a specific poi has been completed
	 * \param object the poi
	 * \param success true if loading was successful, false otherwise
	 */
	void onLoadingPOIComplete(const  metaio::world::MetaioWorldPOI* object, bool success) {};

	/** Inform the engine the loading of a specific poi has been started
	 * \param object the poi
	 */
	void onStartLoadingPOI(const  metaio::world::MetaioWorldPOI* object) {};

	/** Inform the engine that a specific poi will be removed
	 * \param object the poi
	 */
	void onWillRemoveObject(MetaioWorldPOI* object);

	/** Inform the engine that we will remove all content (usually reloading everything)
	 */
	void onWillRemoveContent();


	void callMediaEvent(const std::string& itemID, EAREL_MEDIA_EVENT event);

	bool onNewCameraFrame(metaio::ImageStruct*  cameraFrame)
	{
		return false;
	}

	void onDidUpdatePosition(metaio::LLACoordinate position) {};

	void setEngineIsReady(bool ready);
	
	void onVisualSearchResult(bool success, const std::string& errorMsg,
	                                  const std::vector< metaio::VisualSearchResponse >& response){};

protected:


	/**
	 * Start specified customization type
	 * \param poi POI for which customization is started
	 * \param nodeID customization nodeID, i.e. idle, click, onTargetDetect
	 * \param playAlert true to play an alert before triggering video or website
	 * \param nameToMatch if set, then customization will only be triggered, if its name matches
	 */
	void startCustomization(const MetaioWorldPOI& poi, const std::string& nodeID, bool playAlert = false, const std::string& nameToMatch = "");

	/**
	 * Start specified behavior type
	 * \param  poi POI for which behavior is started
	 * \param behaviorType Behavior type, see MetaioWorldBehaviorType
	 */
	void startBehavior(const MetaioWorldPOI& poi, const MetaioWorldBehaviorType& behaviorType);



	/** Start the idle events of this POI
	 * \param poi the poi
	 * \return true, if event was started, false if it has been started already
	 */
	bool startIdleEventForPOI(MetaioWorldPOI& poi);



	metaio::world::MetaioWorldPOIManager*           m_pPOIManager;                   //!< Pointer to poiManager
	metaio::world::JunaioDataSource*              m_pDatasource;                   //!< Pointer to our datasource
	metaio::world::IMetaioWorldPOIManagerDelegate* 	m_pPOIManagerDelegate;     		 //!< the delegate object that does the work on app side
	std::map< int, double >				m_timeSinceTrackingLost;		//!< Keeps a map of coordinate system IDs and the time since the tracking was lost;
	metaio::common::ITimer*						m_pRuntimeTimer;	//!< Measures the runtime, as getSystemTime is not working robustly.
};


}// namespace arel
}// namespace metaio


#endif
